## Un site dédié à la préparation au CAPES

C'est sur le site https://capes-nsi.org que se trouvent toutes les ressources pour se préparer au CAPES
 
[![Site du jury du CAPES externe NSI](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Que%20faire%20pour%20se%20pr%C3%A9parer%20aux%20concours/Ressources/baniere-capes-nsi-org.png)](https://capes-nsi.org/)

En toute transparence et pour maximiser l'égalité des chances, on y trouve la description des épreuves, la liste des outils logiciels utilisés et des ressources avec des listes de sujets des épreuves pour s'entraîner, les rapports du jury et recommandations pour se positionner.

### Qu'offrons nous en plus ici ?

D'abord … la double formation à la fois aux [fondements de l'informatique](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html) et l'[apprentissage de l'enseignement](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/index.html), à travers deux MOOCs un peu spéciaux. Puis (et surtout) un accompagnement à travers le [forum NSI + SNT](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html) et une possibilité de [nous contacter](https://mooc-nsi-snt.gitlab.io/portail/7_Contact.html) pour aider, à travers ce service public, quiconque souhaite se lancer.

Bien entendu nous nous devons de traiter chacune et chacun de manière égalitaire, nous n'allons donc pas accompagner certaines personnes et pas d'autres, chaque fois que nous pourrons conseiller, partager, aider, nous remettrons immédiatement le contenu en partage, sur le forum ou sur ces pages, tout particulièrement la [foire aux questions]().

## Pas de contenus spécifique ?

Pas besoin :) Toutes les ressources sont là, et la plus value ici est de vous accompagner.
