## 2. Mettre en oeuvre - Animer
### Niveau 2

- Scénariser entièrement une séance, préciser notamment :
    - les différentes phases, durée,
    - l'organisation du tableau, écran, papier
    - l'organisation pédagogique (seul, binôme, groupe...)
    - les attendus
    - les interactions
    - l'activité du professeur
    - la place de la trace écrite
