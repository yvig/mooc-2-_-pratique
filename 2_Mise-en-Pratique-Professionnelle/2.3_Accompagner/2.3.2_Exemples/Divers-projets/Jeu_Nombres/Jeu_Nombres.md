# Projet Jeu Nombres

Jeu à deux joueurs. 

Des nombres de 1 à 10 sont disposés horizontalement. 
Chacun son tour les joueurs prennent un nombre à une extrémité. A la fin on fait la somme des nombres pris et le jouer qui a la somme la plus grande a gagné. 

![exemple](Jeu-Nombres.png)

Vous devez programmer ce jeu en utilisant les [images données](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/archive/master/mooc-2-_-pratique-master.zip?path=2_Mise-en-Pratique-Professionnelle/2.3_Accompagner/2.3.2_Exemples/Divers-projets/Jeu_Nombres).
